const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'cheap-module-eval-source-map',
  module: {
    rules: [
      { test: /\.css$/, loader: 'style-loader!css-loader' },
      { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader' }
    ]
  },
  devServer: {
    contentBase: './public',
    hot: false,
    lazy: false,
    port: 3000,
    disableHostCheck: true,
    proxy: {
      '/**': {  // catch all requests
        target: '/index.html',  // default target
        secure: false,
        bypass(req, res, opt) {
          if (req.url.indexOf('/css/') !== -1
            || req.url.indexOf('/images/') !== -1
            || req.url.indexOf('/fonts/') !== -1
            || req.url.indexOf('/bundle.js') !== -1
            || req.url.indexOf('/favicon.ico') !== -1
          ) {
            let parts = req.url.split('/').filter((v) => {
              return (v !== '');
            });

            let found = false;
            while (!found) {
              const piece = parts.shift();
              if (!piece) {
                parts = ['/'];
                found = true;
              } else if (['css', 'images', 'fonts', 'bundle.js', 'favicon.ico'].includes(piece)) {
                found = true;
                parts.unshift(`/${piece}`);
              }
            }
            return (parts.join('/'));
          }

          if (req.headers.accept.indexOf('html') !== -1) {
            return '/index.html';
          }
        }
      }
    }
  }
});
