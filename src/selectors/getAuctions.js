import { createSelector } from 'reselect';

export const auctionsSelector = state => state.auctions.auctions;

export default createSelector(
  [auctionsSelector],
  (auctions) => {
    return [...auctions.values()];
  }
);
