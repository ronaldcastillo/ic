import Auction from '../models/Auction';

const initialState = {
  auctions: new Map()
};

export default function auctions(state = initialState, action = {}) {
  let auctions;
  switch (action.type) {
    case 'SET AUCTIONS':
      auctions = new Map(state.auctions);
      action.payload.auctions.forEach((auction) => {
        auctions.set(auction.id, new Auction(auction));
      });
      return Object.assign({}, state, {
        auctions
      });
    case 'DECREMENT':
      auctions = new Map(
        [...state.auctions.values()]
          .filter(auction => auction.remainingTime > 0)
          .map(auction => {
            auction.decrement();
            return [auction.id, auction]
          })
      );

      return Object.assign({}, state, {
        auctions: new Map(auctions)
      });
    case 'PLACE NEW BID':
      // We should be calling an API or something, but, that's not on the requirements (no endpoint to do so)
      auctions = new Map(state.auctions);
      const newAuction = auctions.get(action.payload.id);
      newAuction.bids.push({
        amount: newAuction.latestBid.amount + 250,
        channel: 'Web',
        dealership: 'InstaCarro'
      })
    default:
      return state;
  }
};
