import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import auctions from './auctions';

export default combineReducers({
  auctions,
  routing: routerReducer,
});
