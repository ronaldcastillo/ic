const AuctionsActions = {
  fetchAuctions() {
    return {
      type: 'FETCH AUCTIONS'
    }
  },
  initialize() {
    return {
      type: 'INITIALIZE'
    }
  },
  placeNewBid(id) {
    return {
      type: 'PLACE NEW BID',
      payload: {
        id
      }
    }
  }
};

export default AuctionsActions;
