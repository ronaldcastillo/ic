class Auction {
  constructor(properties) {
    this.properties = Object.assign({}, properties);
  }

  get id() {
    return this.properties.id;
  }

  get make() {
    return this.properties.make;
  }

  get model() {
    return this.properties.model;
  }

  get version() {
    return this.properties.version;
  }

  get km() {
    return this.properties.km;
  }

  get year() {
    return this.properties.year;
  }

  get remainingTime() {
    return this.properties.remainingTime;
  }

  get imageUrl() {
    return this.properties.imageUrl;
  }
  
  get bids() {
    return this.properties.bids;
  }

  get title() {
    return `${this.properties.make} ${this.properties.model} ${this.properties.version} ${this.properties.year}`;
  }

  get latestBid() {
    return this.properties.bids.reduce((acc, current) => {
      if (current.amount > acc.amount) {
        return current;
      }
      return acc;
    }, {
      amount: 0,
    })
  }

  decrement() {
    this.properties.remainingTime = this.properties.remainingTime || 0;
    this.properties.remainingTime -= 1;
  }

  set id(id) {
    this.properties.id = id;
  }

  set make(make) {
    this.properties.make = make;
  }

  set model(model) {
    this.properties.model = model;
  }

  set version(version) {
    this.properties.version = version;
  }

  set year(year) {
    this.properties.year = year;
  }

  set km(km) {
    this.properties.km = km;
  }

  set remainingTime(remainingTime) {
    this.properties.remainingTime = remainingTime;
  }

  set imageUrl(imageUrl) {
    this.properties.imageUrl = imageUrl;
  }

  set bids(bids) {
    this.properties.bids = bids;
  }
}

export default Auction;
