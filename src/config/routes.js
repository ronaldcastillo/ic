import React from 'react';
import {
  Router,
  Route,
  IndexRoute
} from 'react-router';

import Application from '../components/layout/Application';
import Auctions from '../containers/auctions/Auctions';

export default (
  <Router>
    <Route path="/" component={Application}>
      <IndexRoute component={Auctions} />
    </Route>
  </Router>
);
