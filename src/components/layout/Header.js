import React from 'react';

const Phone = () => {
  return (
    <div className="Phone">
      <img src="/images/phone.png" alt="InstaCarro.com" />
      <p>(11) 3569-3465</p>
    </div>
  );
}

class Header extends React.Component {
  render() {
    return (
      <div className="HeaderContainer">
        <div className="Header">
          <div>
            <img src="/images/logo.png" alt="InstaCarro.com" className="HasBorderRight Logo" />
          </div>
          <div className="Phone">
            <img src="/images/phone.png" alt="InstaCarro.com" />
          </div>
          <div>
            <p>(11) 3569-3465</p>
          </div>
          <div className="User">
            User
          </div>
        </div>
      </div>
    )
  }
}

export default Header;
