import React from 'react';

class Button extends React.Component {
  render() {
    return (
      <div className="Button" onClick={this.props.onClick}>
        {this.props.children}
      </div>
    );
  }
}

Button.propTypes = {
  onClick: React.PropTypes.func
};

Button.defaultProps = {
  onClick: () => {}
};

export default Button;
