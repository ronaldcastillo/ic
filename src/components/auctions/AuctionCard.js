import React from 'react';
import moment from 'moment';
import 'moment-duration-format';

import './AuctionCard.scss';
import Button from '../common/Button';

const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
});

class AuctionCard extends React.Component {
  render() {
    const {
      auction,
      placeNewBid
    } = this.props;

    return (
      <div className="AuctionCard">
        <div className="Image" style={{ backgroundImage: `url(${auction.imageUrl})` }}>
          <div className="Description">
            <span>Ver Detalhes</span>
          </div>
        </div>
        <div className="Inner">
          <div className="Col-2" style={{ paddingBottom: 0 }}>
            <div>
              <h2>Tempo Restante</h2>
            </div>
            <div>
              <h2>Ultima Oferta</h2>
            </div>
          </div>
          <div className="BidInformation Col-2">
            <div className="TimeLeft HasBorderRight">
              <span>{moment.duration(auction.remainingTime, "seconds").format("h:mm:ss")}</span>
            </div>
            <div className="LastBid">
              <span>R$ {Number(auction.latestBid.amount || 0).toLocaleString()}</span>
            </div>
          </div>
          <div className="CarInformation HasBorderTop">
            {auction.title}
          </div>
          <div className="Additional HasBorderTop Col-2">
            <div className="HasBorderRight">{auction.year}</div>
            <div>{(auction.km / 1000).toFixed(3)} KM</div>
          </div>
          <div className="Actions HasBorderTop">
            <Button onClick={() => placeNewBid(auction.id)}>
              Fazer Oferta
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

AuctionCard.propTypes = {
  auction: React.PropTypes.object.isRequired,
  placeNewBid: React.PropTypes.func.isRequired
};

export default AuctionCard;
