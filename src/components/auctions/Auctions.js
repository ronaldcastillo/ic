import React from 'react';

import AuctionCard from './AuctionCard';

import './Auctions.scss';

class Auctions extends React.Component {
  componentDidMount() {
    return new Promise((resolve, reject) => {
      this.props.fetchAuctions();
    });
  }

  render() {
    if (this.props.auctions.length === 0) {
      return null;
    }

    return (
      <div className="Auctions">
        {
          this.props.auctions.map(auction => {
            return <AuctionCard key={auction.id} auction={auction} placeNewBid={this.props.placeNewBid} />
          })
        }
      </div>
    );
  }
}

Auctions.propTypes = {
  fetchAuctions: React.PropTypes.func.isRequired,
  placeNewBid: React.PropTypes.func.isRequired,
  auctions: React.PropTypes.array.isRequired,
  initialize: React.PropTypes.func.isRequired
};

export default Auctions;
