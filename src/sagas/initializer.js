import { put, takeLatest } from 'redux-saga/effects';

export function* handleInitializer() {
  yield put({ type: 'INITIALIZE AUCTION COUNTER' });
}

export default function* initializer() {
  yield takeLatest('DONE FETCHING AUCTIONS', handleInitializer);
}
