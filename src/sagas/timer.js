import { put, takeLatest } from 'redux-saga/effects';

const delay = (ms) => new Promise(res => setTimeout(res, ms))

export function* decrementAsync() {
  while (true) {
    yield delay(1000);
    yield put({ type: 'DECREMENT' });
  }
}

export default function* timer() {
  yield takeLatest('INITIALIZE AUCTION COUNTER', decrementAsync);
}
