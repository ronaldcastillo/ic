import { call, put, takeLatest } from 'redux-saga/effects';

import AuctionsService from '../services/auctions';

export function* handleFetchAuctions() {
  try {
    yield put({
      type: 'FETCHING AUCTIONS'
    });

    const auctions = yield call(AuctionsService.getAll);

    yield put({
      type: 'SET AUCTIONS',
      payload: {
        auctions
      }
    });

    yield put({
      type: 'DONE FETCHING AUCTIONS'
    });
  } catch (error) {
    yield put({
      type: 'FETCH AUCTIONS FAILED',
      payload: {
        error
      }
    });
  }
}

export default function* fetch_auctions() {
  yield takeLatest(['FETCH AUCTIONS'], handleFetchAuctions);
}
