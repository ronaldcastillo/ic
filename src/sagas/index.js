import initializer from './initializer';
import fetch_auctions from './fetch_auctions';
import timer from './timer';

export default function* rootSaga() {
  yield [
    initializer(),
    fetch_auctions(),
    timer(),
  ];
}
