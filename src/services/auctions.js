export default {
  getAll: (options = {}) => {

    const headers = Object.assign({}, {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'mode': 'cors'
    }, options);

    return fetch('https://s3-sa-east-1.amazonaws.com/config.instacarro.com/recruitment/auctions.json', headers)
      .then(response => {
        if (!response) {
          return Promise.reject(new Error('An error has occurred'));
        }

        return response;
      })
      .then(response => {
        if (response.json) {
          return response.json();
        }

        return null;
      });
  }
};
