import React from 'react';
import { applyMiddleware, compose, createStore } from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';

import sagas from './sagas';
import rootReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();

const middleware = [routerMiddleware(browserHistory),
  sagaMiddleware
];

const store = createStore(
  rootReducer, compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f)
);

sagaMiddleware.run(sagas);

export default store;
