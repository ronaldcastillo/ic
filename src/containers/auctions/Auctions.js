import { connect } from 'react-redux';
import AuctionsActions from '../../actions/AuctionsActions';

import getAuctions from '../../selectors/getAuctions';
import Auctions from '../../components/auctions/Auctions';

const mapStateToProps = (state) => {
  return {
    auctions: getAuctions(state)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAuctions: () => {
      dispatch(AuctionsActions.fetchAuctions());
    },
    initialize: () => {
      dispatch(AuctionsActions.initialize());
    },
    placeNewBid: (id) => {
      dispatch(AuctionsActions.placeNewBid(id));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Auctions);
